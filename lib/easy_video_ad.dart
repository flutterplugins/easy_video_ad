import 'dart:async';

import 'package:flutter/services.dart';

typedef AdLoadedFunction = void Function();
typedef AdOpenedFunction = void Function();
typedef AdStartedFunction = void Function();
typedef AdRewardedFunction = void Function(
    int rewardedAmount, String rewardedType, String rewardedItem);
typedef AdClosedFunction = void Function();
typedef AdLeftApplicationFunction = void Function();
typedef AdFailedToLoadFunction = void Function(int errorCode);
typedef AdCompletedFunction = void Function();
typedef AdEventTriggeredFunction = void Function(String event);
typedef InitializationCompleteFunction = void Function();

class EasyVideoAd {
  static void putInMain() {
    _setNativeMethodCallHandler();
    _subToAdListener();
  }

  static void putInDestroy() {
    _unSubAdListener();
  }

  /*******************************************************************************
  * TITLE: Constants 
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/
  static const MethodChannel _channel = const MethodChannel('easy_video_ad');

  static const EventChannel _easyVideoAdListener =
      const EventChannel('easy_video_ad_listener');

  static const String testVideoId = "ca-app-pub-3940256099942544/5224354917";

  /*******************************************************************************
  * TITLE: on MethodChannel invokes from native code
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/

  ///put this in `main(){}`
  static void _setNativeMethodCallHandler() {
    _channel.setMethodCallHandler((MethodCall call) {
      switch (call.method) {
        case 'onInitializationComplete':
          if (onInitializationComplete != null) onInitializationComplete();
          break;
        default:
      }
      return;
    });
  }

  static InitializationCompleteFunction onInitializationComplete;

  /*******************************************************************************
  * TITLE: lifecycle methods
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/
  ///
  static void pause() => _channel.invokeMethod('pause');
  static void resume() => _channel.invokeMethod('resume');
  static void destroy() => _channel.invokeMethod('destroy');

  /*******************************************************************************
  * TITLE: loadAd in various ways
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/
  ///
  static void loadAd() => _channel.invokeMethod('loadAd');
  static void loadAdWithVideoID(String videoId) =>
      _channel.invokeMethod('loadAdWithVideoID', <String, dynamic>{
        'loadAdWithVideoID_arg_videoID': videoId,
      });
  static void forceLoadAd(String videoId) =>
      _channel.invokeMethod('forceLoadAd', <String, dynamic>{
        'forceLoadAd_arg_videoID': videoId,
      });
  static void loadTestAd() => _channel.invokeMethod('loadTestAd');
  static void forceLoadTestAd() => forceLoadAd(testVideoId);
  /*******************************************************************************
  * TITLE: showAd in various ways
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/
  ///Only executes rewardedVideoAd.show() in native code
  static void showAd() => _channel.invokeMethod('showAd');

  ///Only show ad if rewardedVideoAd.isLoaded() == true
  static void showAdIfLoaded() => _channel.invokeMethod('showAdIfLoaded');

  /*******************************************************************************
  * TITLE: get & set some values
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/
  ///Check if Ad isLoaded
  static Future<bool> isLoaded() => _channel.invokeMethod('isLoaded');

  static Future<String> getMediationAdapterClassName() =>
      _channel.invokeMethod('getMediationAdapterClassName');

  static Future<String> getResponseId() =>
      _channel.invokeMethod('getResponseId');

  static Future<String> getCustomData() =>
      _channel.invokeMethod('getCustomData');

  static Future<void> setCustomData(String customData) => _channel.invokeMethod(
      'setCustomData',
      <String, dynamic>{'setCustomData_arg_customData': customData});

  static Future<String> getUserId() => _channel.invokeMethod('getUserId');

  static Future<void> setUserId(String userId) => _channel.invokeMethod(
      'setUserId', <String, dynamic>{'setUserId_arg_userId': userId});

  static Future<void> setImmersiveMode(bool immersiveMode) =>
      _channel.invokeMethod('setImmersiveMode', <String, dynamic>{
        'setImmersiveMode_arg_immersiveMode': immersiveMode
      });

  static Future<void> setRewardedVideoAd() =>
      _channel.invokeMethod('setRewardedVideoAd');

  static Future<void> setVideoId(String videoId) => _channel.invokeMethod(
      'setVideoId', <String, dynamic>{'setVideoId_arg_videoID': videoId});
  /*******************************************************************************
  * TITLE: EasyVideoAdListener
  * AUTHOR: ObeE
  * DESCRIPTION:
  ********************************************************************************/
  static StreamSubscription _listenerSub;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents onRewardedVideoAdLoaded
  ///
  /// ## Parameters
  ///
  /// * no parameters
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdLoaded = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdLoadedFunction onAdLoaded;
  static int _loadedCount = 0;
  int get loadedCount => _loadedCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewardedVideoAdOpened()`
  ///
  /// ## Parameters
  ///
  /// * no parameters
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdOpened = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdOpenedFunction onAdOpened;
  static int _openedCount = 0;
  int get openedCount => _openedCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewardedVideoStarted()`
  ///
  /// ## Parameters
  ///
  /// * no parameters
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdStarted = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdStartedFunction onAdStarted;
  static int _startedCount = 0;
  int get startedCount => _startedCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewarded(RewardItem rewardItem)`
  ///
  /// ## Parameters
  ///
  /// * 1.Param: `rewardedAmount` = `int rewardedItem.getAmount()`
  ///
  /// * 2.Param: `rewardedType` = `String rewardedItem.getType()`
  ///   This may be null
  ///
  /// * 3.Param: `rewardedItem` = `String rewardedItem.toString()`
  ///   This may be null
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdRewarded = (
  ///       int rewardedAmount,
  ///       String rewardedType,
  ///       String rewardedItem){
  ///         /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdRewardedFunction onAdRewarded;
  static int _rewardedCount = 0;
  int get rewardedCount => _rewardedCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewardedVideoAdClosed()`
  ///
  /// ## Parameters
  ///
  /// * no parameters
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdClosed = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdClosedFunction onAdClosed;
  static int _closedCount = 0;
  int get closedCount => _closedCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewardedVideoAdLeftApplication()`
  ///
  /// ## Parameters
  ///
  /// * no parameters
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdLeftApplication = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdLeftApplicationFunction onAdLeftApplication;
  static int _leftApplicationCount = 0;
  int get leftApplicationCount => _leftApplicationCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewardedVideoAdFailedToLoad(int i)`
  ///
  /// ## Parameters
  ///
  /// * `int errorCode` ( = RewardedVideoAdFailedToLoad ErrorCode)
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdFailedToLoad = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdFailedToLoadFunction onAdFailedToLoad;
  static int _failedToLoadCount = 0;
  int get failedToLoadCount => _failedToLoadCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This represents `onRewardedVideoCompleted()`
  ///
  /// ## Parameters
  ///
  /// * no parameters
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdCompleted = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdCompletedFunction onAdCompleted;
  static int _completedCount = 0;
  int get completedCount => _completedCount;

  /// ## Description
  ///
  /// * You have to put `EasyVideoAd.subToAdListener()` in your `main(){}`,
  ///   in order for this to work
  ///
  /// * This will always be called after the event related methods (e.g. `EasyVideoAd.onAdLoaded`,...)
  ///
  /// ## Parameters
  ///
  /// * `String event` = "onRewardedVideoAdLoaded"
  /// * `String event` = "onRewardedVideoAdOpened"
  /// * `String event` = "onRewardedVideoStarted"
  /// * `String event` = "onRewardedVideoAdClosed"
  /// * `String event` = "onRewarded;" + rewardItem.toString() + ";" + rewardItem.getAmount() + ";" + rewardItem.getType()
  /// * `String event` = "onRewardedVideoAdLeftApplication"
  /// * `String event` = "onRewardedVideoAdFailedToLoad;" + i
  /// * `String event` = "onRewardedVideoCompleted"
  ///
  /// ## Example
  /// {@tool sample}
  ///
  /// ```dart
  /// void main(){
  ///   EasyVideoAd.onAdEventTriggered = (){
  ///       /*some code*/
  ///   };
  /// }
  /// ```
  /// {@end-tool}
  static AdEventTriggeredFunction onAdEventTriggered;

  ///put this in main(){}
  static void _subToAdListener() {
    _listenerSub =
        _easyVideoAdListener.receiveBroadcastStream().listen((onData) {
      _onAdEvent(onData);
      if (onAdEventTriggered != null) onAdEventTriggered(onData);
    });
  }

  ///put this in destroy(){}
  static void _unSubAdListener() {
    _listenerSub.cancel();
  }

  ///Very simple, just executes the Callbacks and increases the Counters
  static void _onAdEvent(String event) {
    int rewardedAmount = 0;
    String rewardedType = "";
    String rewardedItemToString = "";
    int failedToLoadCode = -1;

    //Check if either 'onRewarded' or 'onRewardedVideoAdFailedToLoad' triggered
    if (event.contains(';')) {
      if (event.contains('onRewardedVideoAdFailedToLoad')) {
        // event == "onRewardedVideoAdFailedToLoad;" + i

        List<String> cache = event.split(";");
        event = cache[0];
        failedToLoadCode = int.parse(cache[1]);
      } else {
        // event == "onRewarded;" + rewardItem.toString() + ";" + rewardItem.getAmount() + ";" + rewardItem.getType()

        List<String> cache = event.split(";");
        event = cache[0];
        rewardedItemToString = cache[1];
        rewardedAmount = int.parse(cache[2]);
        rewardedType = cache[3];
      }
    }

    _incCounter(event);
    switch (event) {
      case 'onRewardedVideoAdLoaded':
        if (onAdLoaded != null) onAdLoaded();
        break;
      case 'onRewardedVideoAdOpened':
        if (onAdOpened != null) onAdOpened();
        break;
      case 'onRewardedVideoStarted':
        if (onAdStarted != null) onAdStarted();
        break;
      case 'onRewardedVideoAdClosed':
        if (onAdClosed != null) onAdClosed();
        break;
      case 'onRewarded':
        if (onAdRewarded != null)
          onAdRewarded(rewardedAmount, rewardedType, rewardedItemToString);
        break;
      case 'onRewardedVideoAdLeftApplication':
        if (onAdLeftApplication != null) onAdLeftApplication();
        break;
      case 'onRewardedVideoAdFailedToLoad':
        if (onAdFailedToLoad != null) onAdFailedToLoad(failedToLoadCode);
        break;
      case 'onRewardedVideoCompleted':
        if (onAdCompleted != null) onAdCompleted();
        break;
      default:
    }
  }

  ///Increase the counter of the individual event
  static void _incCounter(String event) {
    switch (event) {
      case 'onRewardedVideoAdLoaded':
        _loadedCount++;
        break;
      case 'onRewardedVideoAdOpened':
        _openedCount++;
        break;
      case 'onRewardedVideoStarted':
        _startedCount++;
        break;
      case 'onRewardedVideoAdClosed':
        _closedCount++;
        break;
      case 'onRewarded':
        _rewardedCount++;
        break;
      case 'onRewardedVideoAdLeftApplication':
        _leftApplicationCount++;
        break;
      case 'onRewardedVideoAdFailedToLoad':
        _failedToLoadCount++;
        break;
      case 'onRewardedVideoCompleted':
        _completedCount++;
        break;
      default:
    }
  }
}
